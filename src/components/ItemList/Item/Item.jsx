import React, { useContext } from 'react';
import PropTypes from 'prop-types';

// css
import styles from './Item.module.scss';

// context
import BasketContext from '../../../store/basket-context';

const Item = (props) => {
    // * Context
    const basketContext = useContext(BasketContext);

    // * Function
    // it return another function from basketContext based on the requirement either add or remove
    const taskHandler = () => {
        if (props.type === 'add') {
            return basketContext.addItem({
                id: props.id,
                title: props.title,
            });
        } else {
            return basketContext.removeItem(props.id);
        }
    };

    return (
        <li className={`${styles.item} ${styles[props.color]}`}>
            <span
                onClick={taskHandler}
                className={`${styles.icon} ${
                    props.type === 'add'
                        ? styles['icon-add']
                        : styles['icon-remove']
                }`}
            >
                {props.icon}
            </span>
            {props.quantity && (
                <span className={styles.text}>{props.quantity}</span>
            )}
            <span className={styles.text}>{props.title}</span>
        </li>
    );
};

// * Validation
Item.propTypes = {
    icon: PropTypes.element.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    quantity: PropTypes.number,
};

export default Item;
