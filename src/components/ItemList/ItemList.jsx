import React, { useContext } from 'react';
import PropTypes from 'prop-types';

// css
import styles from './ItemList.module.scss';

// component
import Item from './Item/Item';

// contexts
import BasketContext from '../../store/basket-context';

const ItemList = (props) => {
    // * Context
    const basketContext = useContext(BasketContext);

    //
    const itemLength = props.items.length !== 0;

    return (
        <div className={styles.list}>
            <div className={styles.header}>
                <div className={styles.icon}>{props.icon}</div>
                <h3>{props.title}</h3>

                {/* Add an disabled class in delete btn */}
                <div
                    onClick={itemLength ? basketContext.clearBasket : () => {}}
                    className={`${
                        styles['delete-icon']
                    }                                                             
        ${!itemLength && styles.disabled}`}
                >
                    {props.deleteIcon}
                </div>
            </div>
            {!itemLength ? (
                basketContext.search.length === 0 ? (
                    <p className={styles.msg}>Your Basket Is Empty!</p>
                ) : (
                    <p className={styles.msg}>
                        Sorry nothing matching you search
                    </p>
                )
            ) : (
                <ul>
                    {props.items.map((item, index) => {
                        return (
                            <Item
                                key={item.id}
                                icon={props.listIcon}
                                title={item.title}
                                id={item.id}
                                // providing item quantity if exist
                                quantity={item.quantity && item.quantity}
                                type={props.type}
                                // providing alternative background color to item
                                color={
                                    index % 2 === 0
                                        ? 'color-grey'
                                        : 'color-dark-grey'
                                }
                            />
                        );
                    })}
                </ul>
            )}
        </div>
    );
};

// * validations
ItemList.propTypes = {
    icon: PropTypes.element.isRequired,
    listIcon: PropTypes.element.isRequired,
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    deleteIcon: PropTypes.element,
    items: PropTypes.array,
};

export default ItemList;
