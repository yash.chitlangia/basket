import React, { useContext } from 'react';

// css
import styles from './Basket.module.scss';

// icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faBasketShopping,
    faTrash,
    faSquareMinus,
} from '@fortawesome/free-solid-svg-icons';

// component
import ItemList from '../ItemList/ItemList';

// context
import BasketContext from '../../store/basket-context';
import searchFilter from '../../utils/search-filter';

const Basket = () => {
    // * Icons
    const icon = <FontAwesomeIcon icon={faBasketShopping} />;
    const deleteIcon = (
        <FontAwesomeIcon className={styles['delete-icon']} icon={faTrash} />
    );
    const listIcon = <FontAwesomeIcon icon={faSquareMinus} />;

    // * Context
    const basketContext = useContext(BasketContext);

    // filter out partially matching search result
    const filteredItems = searchFilter(
        basketContext.items,
        basketContext.search,
    );

    return (
        <section className={styles.basket}>
            <ItemList
                icon={icon}
                title="Basket"
                deleteIcon={deleteIcon}
                items={filteredItems}
                listIcon={listIcon}
                type="remove"
            />
        </section>
    );
};

export default Basket;
