import React, { useContext } from 'react';

// css

// eslint-disable-next-line no-unused-vars
import styles from './Groceries.module.scss';

// components
import ItemList from '../ItemList/ItemList';

// data
import { DUMMY_DATA } from '../../data/GroceriesData';

// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLeaf, faSquarePlus } from '@fortawesome/free-solid-svg-icons';
import BasketContext from '../../store/basket-context';
import searchFilter from '../../utils/search-filter';

const Groceries = () => {
    // * Icons
    const icon = <FontAwesomeIcon icon={faLeaf} />;
    const listIcon = <FontAwesomeIcon icon={faSquarePlus} />;

    // * Context
    const basketContext = useContext(BasketContext);

    // filter out partially matching search result using regex
    const filteredItems = searchFilter(DUMMY_DATA, basketContext.search);

    return (
        <section>
            <ItemList
                icon={icon}
                title="Groceries"
                items={filteredItems}
                listIcon={listIcon}
                type="add"
            />
        </section>
    );
};

export default Groceries;
