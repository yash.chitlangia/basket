import React from 'react';

// css
import styles from './Footer.module.scss';

// icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCopyright } from '@fortawesome/free-solid-svg-icons';

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <FontAwesomeIcon className={styles.icon} icon={faCopyright} />
            <p>All Right reserved.</p>
        </footer>
    );
};

export default Footer;
