import React from 'react';

// css
import styles from './Header.module.scss';

// icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBasketShopping } from '@fortawesome/free-solid-svg-icons';

const Header = () => {
    return (
        <header className={styles.header}>
            <FontAwesomeIcon className={styles.icon} icon={faBasketShopping} />
            <h1>Hello, Basket!!</h1>
        </header>
    );
};

export default Header;
