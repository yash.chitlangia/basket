import React, { useContext, useState } from 'react';

// css
import styles from './Search.module.scss';

// context
import BasketContext from '../../store/basket-context';

const Search = () => {
    // * States
    const [enteredText, setEnteredText] = useState('');

    // * Context
    const basketContext = useContext(BasketContext);

    // * Functions

    // fun to update the value of enteredText state from the value of search field
    const changeHandler = (e) => {
        const searchValue = e.target.value.trim(' ');
        setEnteredText(searchValue);

        // updating the value in context only if the char length > 3
        // or if user remove all text from the search field
        if (searchValue.length > 3 || searchValue.length === 0) {
            basketContext.searchFilter(searchValue);
        }
    };

    return (
        <div className={styles.search}>
            <input
                onChange={changeHandler}
                type="text"
                name="search"
                id="search"
                value={enteredText}
                placeholder="Search..."
            />
        </div>
    );
};

export default Search;
