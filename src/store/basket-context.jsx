import React, { createContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

export function BasketContextProvider(props) {
    //* states
    const [items, setItems] = useState([]);
    const [search, setSearch] = useState('');

    //* persisting storage

    // on component first render get the data from localStorage
    useEffect(() => {
        setItems(JSON.parse(window.localStorage.getItem('basket')));
    }, []);

    // sync the data to localStorage on any change in items state
    useEffect(() => {
        window.localStorage.setItem('basket', JSON.stringify(items));
    }, [items]);

    //* functions

    // fun to increment quantity / add item to basket
    const addItemHandler = (item) => {
        // check if item exists
        const existingItem = items.find((element) => element.id === item.id);

        if (existingItem) {
            // finding index of existing item to replace it
            const existingItemIndex = items.findIndex((element) => {
                return element.id === item.id;
            });

            // increment the quantity of item
            const newItem = { ...item, quantity: existingItem.quantity + 1 };
            const updatedItems = [...items];
            updatedItems[existingItemIndex] = newItem;
            setItems(updatedItems);
        }
        // add the new item
        else {
            const newItem = {
                ...item,
                quantity: 1,
            };

            setItems((prevState) => {
                return [...prevState, newItem];
            });
        }
    };

    // fun to decrement quantity / remove the item from basket
    const removeItemHandler = (itemId) => {
        // check if item array is not empty
        if (items.length > 0) {
            // find if item exists
            const existingItem = items.find((element) => element.id === itemId);

            if (existingItem) {
                // check if item quantity is 1
                if (existingItem.quantity === 1) {
                    // remove the entire item
                    // and set the new array
                    setItems((prevState) => {
                        return prevState.filter(
                            (element) => element.id !== itemId,
                        );
                    });
                }
                // if quantity is more than 1
                else {
                    // finding index of existing item to replace it
                    const existingItemIndex = items.findIndex((element) => {
                        return element.id === itemId;
                    });
                    // decrement the quantity of item

                    const newItem = {
                        ...existingItem,
                        quantity: existingItem.quantity - 1,
                    };
                    const updatedItems = [...items];
                    updatedItems[existingItemIndex] = newItem;
                    setItems(updatedItems);
                }
            }
        }
    };

    // fun to remove all the items from the basket
    const clearBasketHandler = () => {
        setItems([]);
    };

    // fun to set search value
    const searchFilter = (data) => {
        setSearch(data);
    };

    return (
        <BasketContext.Provider
            value={{
                items: items,
                search: search,
                addItem: addItemHandler,
                removeItem: removeItemHandler,
                clearBasket: clearBasketHandler,
                searchFilter: searchFilter,
            }}
        >
            {/* // TODO => Find a way to solve below problem of eslint validation on children  */}
            {/* eslint-disable-next-line react/prop-types */}
            {props.children}
        </BasketContext.Provider>
    );
}

const BasketContext = createContext({
    items: [],
    search: '',
    addItem: () => {},
    removeItem: () => {},
    clearBasket: () => {},
    searchFilter: () => {},
});

BasketContext.propTypes = {
    children: PropTypes.element,
};

export default BasketContext;
