// func accepts the array of items and the search keyword to filter out and return list containing partially matching titles

const searchFilter = (list, searchKeyword) => {
    const regex = new RegExp(searchKeyword, 'i');
    const filteredList = list.filter((x) => regex.test(x.title));
    return filteredList;
};

export default searchFilter;
