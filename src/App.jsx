import React from 'react';

// css
import './App.scss';

// components
import Basket from './components/Basket/Basket';
import Groceries from './components/Groceries/Groceries';
import Footer from './components/Layout/Footer/Footer';
import Header from './components/Layout/Header/Header';
import Search from './components/Search/Search';

// context
import { BasketContextProvider } from './store/basket-context';

function App() {
    return (
        <BasketContextProvider>
            <Header />
            <main>
                <Search />
                <div className="container">
                    <div>
                        <Groceries />
                    </div>
                    <div>
                        <Basket />
                    </div>
                </div>
            </main>
            <Footer />
        </BasketContextProvider>
    );
}

export default App;
